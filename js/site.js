$.fn.exists = function() {
  return this.length !== 0;
};

var csub = csub || {};

csub.get_form_data = function(form) {
  var data = {};
  $.each(form.serializeArray(), function(i, field) {
    data[field.name] = field.value;
  });
  return data
}

csub.submit_authkey_request = function() {
  var form = $(this);
  if (form.data('submitted')) { return false; }

  var button = form.find('input[type="submit"]');
  button.button('loading');

  var _status = form.parents('.container').parent().find('.alert');
  var request = csub.get_form_data(form);
  var url = '/api/users/' + request['uid'] + '/password/reset_authorization_request';
  $.post(url, {},
    function(data) {
      form.data('submitted', true);
      _status
        .empty()
        .removeClass()
        .addClass('alert alert-block alert-success')
        .append($('#authkey_alert_success').html());
      clearTimeout(_status.data('timer'));

      form.parent().fadeOut('fast', function() { _status.fadeIn('fast') });
    }
  )
  .error(function(jqXHR, textStatus, errorThrown) {
    button.button('reset');
    _status
      .empty()
      .removeClass()
      .addClass('alert alert-error')
      .append('<strong>Error.</strong> ' + errorThrown);

    _status.complete = function() {
      e = $(this);
      clearTimeout(e.data('timer'));
      var timer = setTimeout(function() { e.slideUp('fast') }, 3000);
      e.data('timer', timer);
    }
    if ( _status.is(':visible') && !_status.is(':animated')) {
      _status.effect('shake', {distance: 2}, 50, _status.complete);
    }
    else {
      _status.slideDown('fast', _status.complete);
    }
  });

  return false;
};

var accounts = accounts || {};
accounts.recovery = {};
accounts.recovery.authkey_request = {};
accounts.recovery.authkey_request.init = function() {
  accounts.recovery.status_timer = null;
  $('#authkey_request_form').submit(csub.submit_authkey_request);
};

accounts.recovery.reset_password = {};
accounts.recovery.reset_password.init = function() {
  $('#submit').prop('disabled', true);

  $('#password_confirm').focus(function() {
    $(this).unbind('focus');
    $('input[type="password"]').keyup(function() {
      setTimeout(accounts.recovery.reset_password.check_inputs, 10);
    });
    accounts.recovery.reset_password.check_inputs();
  });

  $('#password_form').submit(function() {
    $('#submit').button('loading');
    $('input').prop('readonly', true);

    var url = '/api/password/reset_request';
    $.post(url, $('#password_form').serialize(),
      function(data) {
        var status_url = data;
        accounts.recovery.reset_password.status_poll(status_url);
      }
    );

    return false;
  });
};

accounts.recovery.reset_password.check_inputs = function() {
  var set_controls = function(element, css_class, message) {
    element.siblings('.help-inline').text(message);
    element.closest('.control-group')
      .removeClass('success info warning error')
      .addClass(css_class);
  };

  if (!$('#password').val()) {
    set_controls($('#password'), 'error', "Password cannot be empty");
  }
  else {
    set_controls($('#password'), 'success', "Password okay");
  }

  if ($('#password_confirm').val() != $('#password').val()) {
    set_controls($('#password_confirm'), 'error', "Passwords do not match");
  }
  else {
    set_controls($('#password_confirm'), 'success', "Passwords match");
  }

  if ($('.control-group').hasClass('error')) {
    $('#submit').prop('disabled', true);
  }
  else {
    $('#submit').prop('disabled', false);
  }
}

accounts.recovery.reset_password.status_poll = function(status_url) {
  $.get(status_url, function(data, textStatus, jqXHR) {
    console.log(data.status);
    if (data.status == 'pending') {
      setTimeout(function() {
        accounts.recovery.reset_password.status_poll(status_url);
      }, 1000);
    }
    else {
      $('#content').append($('<p id="status"></p>').hide());
      $('#status').text(data.status);
      if (data.status == 'completed') {
        $('#status').removeClass().addClass('alert alert-success');
      }
      else {
        $('#status').removeClass().addClass('alert alert-error');
      }
      $('#password_form').fadeOut('fast', function() { $('#status').show(); });
    }
  });
};

var login = login || {};
login.init = function() {
  $('.form-popper').each(function() {
    $this = $(this);
    var form_id = $this.attr('id') + '_popover_form';
    var content_selector = '#' + $this.attr('data-content-div');
    $this.popover({
      content: function() {
        var content = $(content_selector);
        content.find('form').attr('id', form_id);
        return content.html();
      },
      trigger: 'manual',
    });
    $this.on('click', function(e) {
      $(this).popover('toggle');
      $('#' + form_id + ' input').focus();
      return false;
    });
  });

  $('html').on('click', function(e) {
    if (!$(e.target).parents('.popover').exists()) {
      $('.popper').each(function() {
        var popper = $(this);
        var is_visible = popper.data('popover').tip().hasClass('in');
        if (is_visible) {
          popper.popover('hide');
        }
      });
    }
  });

  $('body').on('submit', '#account_recovery_popover_form', csub.submit_authkey_request);
  $('body').on('submit', '#account_search_popover_form', function() {
    var form = $(this)
    params = csub.get_form_data(form)
    $.get(
      '/api/users?employeeNumber=' + params.search,
      function(data, textStatus, jqXHR) {
        console.log(data);
        
        $('.popper').popover('hide');

        var input = $('#j_username');
        input.val(data.uid[0]);
        input.stop(true, true);
        input.effect("highlight", {}, 3000);

        $('#j_password').focus();
      }
    )
    .error(function(jqXHR, textStatus, errorThrown) {
      _alert = form.parent().find('.alert');
      _alert
        .empty()
        .removeClass()
        .addClass('alert alert-error')
        .append('<strong>Error.</strong> ' + errorThrown);

      if (_alert.is(':visible') && !_alert.is(':animated')) {
        _alert.effect('shake', {distance: 2}, 50);
      }
      else {
        _alert.show();
      }
    })

    return false;
  });
};
