$packages = [
  'devscripts',
  'debhelper',
]
package { $packages: ensure => installed }
