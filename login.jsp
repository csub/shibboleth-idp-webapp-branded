<!DOCTYPE html>

<%@ page import="edu.internet2.middleware.shibboleth.idp.authn.LoginContext" %>
<%@ page import="edu.internet2.middleware.shibboleth.idp.session.*" %>
<%@ page import="edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper" %>
<%@ page import="org.opensaml.saml2.metadata.*" %>

<html>
  <head>
    <title>California State University Bakersfield Shibboleth IdP - Login</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.9.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" />

    <script type="text/javascript" src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/css/bootstrap-combined.min.css">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/css/site.css" />

    <script src="<%= request.getContextPath() %>/js/site.js" type="text/javascript"></script>
    <script type="text/javascript">$(document).ready(login.init)</script>
  </head>

  <body>
    <header class="jumbotron"></header>

    <div class="container">
      <div id="content" class="container">
        <% if (request.getAttribute("actionUrl") != null) { %>
        <form id="login_form" class="form-horizontal" action="<%= request.getAttribute("actionUrl") %>" method="post">
        <% } else { %>
        <form id="login_form" class="form-horizontal" action="j_security_check" method="post">
        <% } %>
          <div class="control-group">
            <% if (request.getParameter("j_username") != null) { %>
            <input id="j_username" name="j_username" type="text" placeholder="netid (e.g. jdoe)" tabindex="1" pattern="[a-z].*" value="<%= request.getParameter("j_username") %>" />
            <% } else { %>
            <input id="j_username" name="j_username" type="text" placeholder="netid (e.g. jdoe)" tabindex="1" pattern="[a-z].*" />
            <% } %>
            <a id="account_search" class="popper form-popper" href="https://csub.edu/accounts/lookup/" data-title="Search for your account" data-content-div="account_search_popover_content" data-placement="bottom">Don't know your NetID?</a>
          </div>

          <div class="control-group">
            <% if (request.getParameter("j_username") != null) { %>
            <input id="j_password" name="j_password" type="password" placeholder="password" tabindex="2" autofocus />
            <% } else { %>
            <input id="j_password" name="j_password" type="password" placeholder="password" tabindex="2" />
            <% } %>
            <a id="account_recovery" href="https://csub.edu/accounts/">Forgot your password?</a>
          </div>

          <div class="control-group">
            <input id="submit" class="btn btn-primary" type="submit" name="submit" value="Login" />
          </div>
        </form>

        <% if ("true".equals(request.getAttribute("loginFailed"))) { %>
        <div class="alert alert-error">Authentication Failed</div>
        <% } %>
      </div>

      <div id="status" class="container" hidden="true"></div>

      <div id="account_search_popover_content" hidden="true">
        <div class="container">
          Enter 9-digit student or employee identification number below
          <form class="form-horizontal" action="" method="post">
            <div class="input-append">
              <input class="input-small" type="text" name="search" placeholder="012345678" maxlength="9" minlength="9" pattern="[0-9]{9}" />
              <button class="btn btn-primary" type="submit">Search</button>
            </div>
          </form>
          <div class="alert" hidden="true"></div>
        </div>
      </div>

    </div>
  </body>
</html>
