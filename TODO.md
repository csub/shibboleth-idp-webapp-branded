 * Fix visual anomaly under IE9 (possibly other versions?)
    - IE9 doesn't properly hide div with hidden attribute. Setting CSS style="display: none" seems to fix this.
